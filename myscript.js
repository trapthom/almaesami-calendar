(function () {

// Checking page title
    if (document.title.indexOf("Riepilogo Esami Studente") != -1) {
        //Creating Elements
        $(document).ready(function () {

            $(".statoProvaPrenotata").each(function (index) {

                var newButton = $("<a target=\"_blank\" class=\"button\" title=\"Aggiungi Esame su Google Calendar\" style=\"cursor: pointer;\">+</a>");
                var container = $(this).closest('tr').children(".nowrap");
                container.append(newButton);

                var title = $(this).closest('tr').parent().closest('tr').prev('tr').children('.colonna:eq(2)').text().trim();
                var location = $(this).closest('tr').parent().closest('tr').prev('tr').children('.colonna:eq(3)').attr('title').trim();
                var dateTimeText = $(this).next('.dataProvaPrenotata').text().trim();

                var day = dateTimeText.substring(0, 2);
                var month = dateTimeText.substring(3, 5);
                var year = dateTimeText.substring(6, 10);

                var hour = dateTimeText.substring(dateTimeText.length - 5, dateTimeText.length - 3);
                var minute = dateTimeText.substring(dateTimeText.length - 2, dateTimeText.length);

                var dateStart = year + '' + month + '' + day;

                // conversione in utc (per google calendar) -2
                var timeStart = (Number(hour) - 2) + '' + minute + '00';
                var timeEnd = (Number(hour)) + '' + minute + '00';

                //Gli orari per il template google devono avere 6 cifre -> paddo con 0
                if (timeStart.length == 5) {
                    timeStart = '0' + timeStart;
                }
                if (timeEnd.length == 5) {
                    timeStart = '0' + timeStart;
                }

                var detail = $(this).text();

                var url = $(container.children()[1]).attr('href');
                if(!url) return;

                var attributeMap = {};
                jQuery.ajax({
                    url: url,
                    success: function (result) {
                        $(result).find('.etichetta').each(function () {
                            var k = $(this).text().replace('*', '').trim();
                            attributeMap[k] = $(this).parent().children('.campo').text().trim();
                        });

                    },
                    async: false
                });
                if (attributeMap['Luogo'] != null) {
                    location += ' - ' + attributeMap['Luogo'];
                }
                detail = 'Tipo esame: ' + attributeMap['Modalità svolgimento'] + "%0A";
                detail += 'Professore: ' + attributeMap['Docente'].toLowerCase() + "%0A";
                detail += 'Posizione: ' + attributeMap['Posizione in lista'] + "%0A";

                location = toTitleCase(location.toLowerCase());
                detail = toTitleCase(detail.toLowerCase());
                title = toTitleCase(title.toLowerCase());

                newButton.click(function () {
                    goToCalendar(title, dateStart, timeStart, dateStart, timeEnd, detail, location);
                });
            });

            function goToCalendar(title, dateStart, timeStart, dateEnd, timeEnd, detail, location) {
                window.open('https://www.google.com/calendar/render?action=TEMPLATE&text=' + title + '&dates=' + dateStart + 'T' + timeStart + 'Z/' + dateStart + 'T' + timeEnd + 'Z&details=' + detail + '&location=' + location + '&pli=1&uid=&sf=true&output=xml#f', '_blank');
            }

            function toTitleCase(str) {
                return str.replace(/(?:^|\s)\w/g, function (match) {
                    return match.toUpperCase();
                });
            }

        });

    }
}());